# Grafana

To run grafana, run ./grafana-server.

The local dashboard will be located at localhost:3050.

Local credentials for the dashboard are admin/admin. You need Prometheus and LocalGalaxy running in order for it to be useful.